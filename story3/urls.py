from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
	path('', TemplateView.as_view(template_name = 'story3/index.html'), name = 'index'),
	path('gallery/', TemplateView.as_view(template_name = 'story3/gallery.html'), name = 'gallery'),
]

app_name = 'story3'