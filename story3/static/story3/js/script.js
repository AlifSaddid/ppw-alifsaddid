/* Profile Section elements */
const profileh1 = document.querySelector("section#profile h1");
const profileAboutMe = document.querySelector("#about-me");
const profilePhoto = document.querySelector("#photo");
const profileDetails = document.querySelector("#details");

/* Experiences Section elements */
const experiencesH1 = document.querySelector("section#experiences h1");
const experiencesEduH3 = document.querySelector("div.educations h3");
const experiencesCarH3 = document.querySelector("div.careers h3");

/* Abilities Section elements */
const abilitesH1 = document.querySelector("section#abilities h1");

/* Projects Section element */
const projectsH1 = document.querySelector("section#projects h1");
const projectsP  = document.querySelector("section#projects p");

/* Hobbies Section element */
const hobbiesH1 = document.querySelector("section#hobbies h1");
const hobbiesP  = document.querySelector("section#hobbies p");

/* Footer Section Element */
const footerP = document.querySelector("footer p");

$(window).scroll(function(){
    let wScroll = $(this).scrollTop();

    

    // Landing Element for Profile Section
    if (wScroll > 200){
        profileh1.classList.remove("hide-up");
        profileh1.classList.add("muncul-down");
    }
    if (wScroll < 200){
        profileh1.classList.add("hide-up");
        profileh1.classList.remove("muncul-down");
    }

    if (wScroll > 400){
        profileAboutMe.classList.remove("hide-up");
        profileAboutMe.classList.add("muncul-down");
    }
    if (wScroll < 400){
        profileAboutMe.classList.add("hide-up");
        profileAboutMe.classList.remove("muncul-down");
    }

    if (wScroll > 400){
        profilePhoto.classList.remove("hide-up");
        profilePhoto.classList.add("muncul-down");
    }
    if (wScroll < 400){
        profilePhoto.classList.add("hide-up");
        profilePhoto.classList.remove("muncul-down");
    }

    if (wScroll > 400){
        profileDetails.classList.remove("hide-up");
        profileDetails.classList.add("muncul-down");
    }
    if (wScroll < 400){
        profileDetails.classList.add("hide-up");
        profileDetails.classList.remove("muncul-down");
    }

    // Landing element for Experiences section

    if (wScroll > 700){
        experiencesH1.classList.remove("hide-up");
        experiencesH1.classList.add("muncul-down");
    }
    if (wScroll < 700){
        experiencesH1.classList.add("hide-up");
        experiencesH1.classList.remove("muncul-down");
    }

    if (wScroll > 750){
        experiencesEduH3.classList.remove("hide-up");
        experiencesEduH3.classList.add("muncul-down");
    }
    if (wScroll < 750){
        experiencesEduH3.classList.add("hide-up");
        experiencesEduH3.classList.remove("muncul-down");
    }

    if (wScroll > 800){
        $("div.educations div.row").each(function(e){
            setTimeout(function(){
                $("div.educations div.row").eq(e).addClass("muncul-down");
                $("div.educations div.row").eq(e).removeClass("hide-up");
            }, 250*(e + 1));
        })
    }

    if (wScroll < 800){
        $("div.educations div.row").each(function(e){
            setTimeout(function(){
                $("div.educations div.row").eq(e).removeClass("muncul-down");
                $("div.educations div.row").eq(e).addClass("hide-up");
            }, 50*(e + 1));
        })
    }

    if (wScroll > 1050){
        experiencesCarH3.classList.remove("hide-up");
        experiencesCarH3.classList.add("muncul-down");
    }
    if (wScroll < 1050){
        experiencesCarH3.classList.add("hide-up");
        experiencesCarH3.classList.remove("muncul-down");
    }

    if (wScroll > 1150){
        $("div.educations div.row").each(function(e){
            setTimeout(function(){
                $("div.careers div.row").eq(e).addClass("muncul-down");
                $("div.careers div.row").eq(e).removeClass("hide-up");
            }, 250*(e + 1));
        })
    }

    if (wScroll < 1150){
        $("div.careers div.row").each(function(e){
            setTimeout(function(){
                $("div.careers div.row").eq(e).removeClass("muncul-down");
                $("div.careers div.row").eq(e).addClass("hide-up");
            }, 50*(e + 1));
        })
    }
    
    // Landing elements for Ability Section
    if (wScroll > 1400){
        abilitesH1.classList.remove("hide-up");
        abilitesH1.classList.add("muncul-down");
    }
    if (wScroll < 1400){
        abilitesH1.classList.add("hide-up");
        abilitesH1.classList.remove("muncul-down");
    }

    if (wScroll > 1600){
        $("section#abilities div.row").each(function(e){
            setTimeout(function(){
                $("section#abilities div.row").eq(e).addClass("muncul-down");
                $("section#abilities div.row").eq(e).removeClass("hide-up");
            }, 100*(e + 1));
        })
    }

    if (wScroll < 1600){
        $("section#abilities div.row").each(function(e){
            setTimeout(function(){
                $("section#abilities div.row").eq(e).removeClass("muncul-down");
                $("section#abilities div.row").eq(e).addClass("hide-up");
            }, 50*(e + 1));
        })
    }

    // Landing elements for projects section
    if (wScroll > 2000){
        projectsH1.classList.remove("hide-up");
        projectsH1.classList.add("muncul-down");
    }
    if (wScroll < 2000){
        projectsH1.classList.add("hide-up");
        projectsH1.classList.remove("muncul-down");
    }

    if (wScroll > 2100){
        projectsP.classList.remove("hide-up");
        projectsP.classList.add("muncul-down");
    }
    if (wScroll < 2100){
        projectsP.classList.add("hide-up");
        projectsP.classList.remove("muncul-down");
    }

    if (wScroll > 2300){
        $("section#projects div.col-md-6").each(function(e){
            setTimeout(function(){
                $("section#projects div.col-md-6").eq(e).addClass("muncul-down");
                $("section#projects div.col-md-6").eq(e).removeClass("hide-up");
            }, 150*(e + 1));
        })
    }

    if (wScroll < 2300){
        $("section#projects div.col-md-6").each(function(e){
            setTimeout(function(){
                $("section#projects div.col-md-6").eq(e).removeClass("muncul-down");
                $("section#projects div.col-md-6").eq(e).addClass("hide-up");
            }, 50*(e + 1));
        })
    }

    // Landing elements for Hobbies section
    if (wScroll > 2500){
        hobbiesH1.classList.remove("hide-up");
        hobbiesH1.classList.add("muncul-down");
    }
    if (wScroll < 2500){
        hobbiesH1.classList.add("hide-up");
        hobbiesH1.classList.remove("muncul-down");
    }

    if (wScroll > 2600){
        hobbiesP.classList.remove("hide-up");
        hobbiesP.classList.add("muncul-down");
    }
    if (wScroll < 2600){
        hobbiesP.classList.add("hide-up");
        hobbiesP.classList.remove("muncul-down");
    }

    if (wScroll > 2800){
        $("section#hobbies div.col-md-4").each(function(e){
            setTimeout(function(){
                $("section#hobbies div.col-md-4").eq(e).addClass("muncul-down");
                $("section#hobbies div.col-md-4").eq(e).removeClass("hide-up");
            }, 150*(e + 1));
        })
    }

    if (wScroll < 2800){
        $("section#hobbies div.col-md-4").each(function(e){
            setTimeout(function(){
                $("section#hobbies div.col-md-4").eq(e).removeClass("muncul-down");
                $("section#hobbies div.col-md-4").eq(e).addClass("hide-up");
            }, 50*(e + 1));
        })
    }

    // Landing elements for footer section
    if (wScroll > 2900){
        footerP.classList.remove("hide-up");
        footerP.classList.add("muncul-down");
    }
    if (wScroll < 2900){
        footerP.classList.add("hide-up");
        footerP.classList.remove("muncul-down");
    }

    if (wScroll > 2900){
        $("section#footer a").each(function(e){
            setTimeout(function(){
                $("section#footer a").eq(e).addClass("muncul-down");
                $("section#footer a").eq(e).removeClass("hide-up");
            }, 150*(e + 1));
        })
    }

    if (wScroll < 2900){
        $("section#footer a").each(function(e){
            setTimeout(function(){
                $("section#footer a").eq(e).removeClass("muncul-down");
                $("section#footer a").eq(e).addClass("hide-up");
            }, 50*(e + 1));
        })
    }

});
