from django.test import TestCase, Client
from django.urls import resolve

from .views import index, tambah
from .models import Aktivitas, Peserta

# Create your tests here.
class Story6Test(TestCase):
    
    # Url testcases
    def test_url_is_exist_story6(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_is_exist_story6_tambah_aktivitas(self):
        response = Client().get('/story6/tambah/')
        self.assertEqual(response.status_code, 302)

    # View testcases
    def test_view_story6_is_using_function_index(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)

    def test_view_story6_tambah_is_using_function_tambah(self):
        found = resolve('/story6/tambah/')
        self.assertEqual(found.func, tambah)

    # Template testcases
    def test_template_story6_using_index_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/index.html')  

    def test_template_story6_can_save_aktivitas_POST_request(self):
        response = Client().post(
            '/story6/tambah/', 
            data = {
                'nama': 'Mengerjakan TK'
            }
        )
        count_all_available_aktivitas = Aktivitas.objects.all().count()
        self.assertEqual(count_all_available_aktivitas, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/#index')

        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn('Mengerjakan TK', html_response)

    def test_template_story6_can_delete_aktivitas_from_POST_request(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
        id = aktivitas_baru.id
        response = Client().post(
            '/story6/hapus/' + str(id) + '/',
        )
        
        count_all_available_aktivitas = Aktivitas.objects.all().count()
        self.assertEqual(count_all_available_aktivitas, 0)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/#index')

    def test_template_story6_cannot_delete_aktivitas_from_GET_request(self):
        response = Client().get(
            '/story6/hapus/0/',
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/#index')  

    def test_template_story6_can_save_peserta_from_POST_request(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
        id = aktivitas_baru.id
        response = Client().post(
            '/story6/peserta/tambah/', 
            data = {
                'nama': 'Muhammad Alif Saddid',
                'aktivitas': id,
            }
        )

        count_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(count_all_available_peserta, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/#index')

        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn('Muhammad Alif Saddid', html_response)
    
    def test_template_story6_can_delete_peserta_from_POST_request(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
        peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)
        id = peserta_baru.id
        response = Client().post(
            '/story6/peserta/hapus/' + str(id) + '/',
        )
        
        count_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(count_all_available_peserta, 0)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/#index')

    def test_template_story6_cannot_delete_peserta_from_GET_request(self):
        response = Client().get(
            '/story6/peserta/hapus/0/',
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/story6/#index')

    # Model testcases
    def test_model_aktivitas_create(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')

        count_all_available_aktivitas = Aktivitas.objects.all().count()
        self.assertEqual(count_all_available_aktivitas, 1)

    def test_model_peserta_create(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
        peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)

        count_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(count_all_available_peserta, 1)

    def test_model_aktivitas_delete(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
        aktivitas_baru.delete()

        count_all_available_aktivitas = Aktivitas.objects.all().count()
        self.assertEqual(count_all_available_aktivitas, 0)

    def test_model_peserta_delete(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
        peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)
        peserta_baru.delete()

        count_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(count_all_available_peserta, 0)

    def test_model_peserta_delete_cascade(self):
        aktivitas_baru = Aktivitas.objects.create(nama = 'Mengerjakan TK PPW')
        peserta_baru = Peserta.objects.create(nama = 'Muhammad Alif Saddid', aktivitas = aktivitas_baru)
        aktivitas_baru.delete()

        count_all_available_aktivitas = Aktivitas.objects.all().count()
        count_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(count_all_available_peserta, 0)
        self.assertEqual(count_all_available_aktivitas, 0)