from django.db import models

class Aktivitas(models.Model):
    nama = models.CharField(max_length = 100, default = '')

class Peserta(models.Model):
    nama = models.CharField(max_length = 100, default = '')
    aktivitas = models.ForeignKey(Aktivitas, on_delete = models.CASCADE)
