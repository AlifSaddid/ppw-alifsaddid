from django.shortcuts import render, redirect, resolve_url
from .models import Aktivitas, Peserta
from .forms import AktivitasForm, PesertaForm

# Create your views here.
def index(request):
    pesertaForm = PesertaForm()
    aktivitasForm = AktivitasForm()
    activities = Aktivitas.objects.all()
    lst = [[i, Peserta.objects.filter(aktivitas = i)] for i in activities]
    context = {
        'activities': lst,
        'Form': pesertaForm,
        'AktivitasForm': aktivitasForm,
    }
    return render(request, 'story6/index.html', context)

def tambah(request):
    form = AktivitasForm(request.POST or None)
    if (request.method == 'POST'):
        if (form.is_valid):
            form.save()
    return redirect(resolve_url('story6:index') + '#index')


def hapus(request, id):
    if (request.method == "GET"):
        return redirect(resolve_url('story6:index') + '#index')

    aktivitas = Aktivitas.objects.get(id = id)
    aktivitas.delete() 
    return redirect(resolve_url('story6:index') + '#index')

def peserta_tambah(request):
    form = PesertaForm(request.POST or None)
    if (request.method == 'POST'):
        if (form.is_valid):
            form.save()
    return redirect(resolve_url('story6:index') + '#index')

def peserta_hapus(request, id):
    if (request.method == "GET"):
        return redirect(resolve_url('story6:index') + '#index')

    peserta = Peserta.objects.get(id = id)
    peserta.delete()
    return redirect(resolve_url('story6:index') + '#index')