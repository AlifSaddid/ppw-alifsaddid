from django.urls import path

from .views import index, tambah, hapus, peserta_tambah, peserta_hapus

urlpatterns = [
	path('', index, name = 'index'),
    path('tambah/', tambah, name = 'tambah'),
    path('hapus/<int:id>/', hapus, name = 'hapus'),
    path('peserta/tambah/', peserta_tambah, name = 'peserta_tambah'),
    path('peserta/hapus/<int:id>/', peserta_hapus, name = 'peserta_hapus'),
]

app_name = 'story6'