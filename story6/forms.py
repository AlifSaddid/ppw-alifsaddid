from django import forms
from .models import Aktivitas, Peserta

class AktivitasForm(forms.ModelForm):
    class Meta:
        model = Aktivitas 
        fields = [
            'nama'
        ]

        widgets = {
            'nama': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan nama aktivitas'
                }
            )
        }

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'nama',
            'aktivitas'
        ]

        widgets = {
            'nama': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Tambahkan peserta'
                }
            )
        }