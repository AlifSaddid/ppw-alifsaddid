//Event pada saat link diklik
$(".page-scroll").on("click", function(event){
	
	var href = $(this).attr("href");
	
	var section = $(href);
	
	$("html").animate({
		scrollTop: section.offset().top - 55
	}, 500, "swing");
	
	event.preventDefault();
	
});

//Jumbotron Parallax && Landing elements

$(window).scroll(function(){
	
	var wScroll = $(this).scrollTop();
	
	//Jumbotron Parallax
	$(".jumbotron img").css({
		"transform" : "translate(0," + wScroll/4 + "%)"
	});
	
	$(".jumbotron h1").css({
		"transform" : "translate(0," + wScroll/2 + "%)"
	});
	
	$(".jumbotron p").css({
		"transform" : "translate(0," + wScroll + "%)"
	});
	
	//Portfolio landing
	if (wScroll >= 500){
		$(".portfolio .img-thumbnail").each(function(e){
			setTimeout(function(){
				$(".portfolio .img-thumbnail").eq(e).addClass("muncul");
			}, 100*(e+1));
		});
	}
	
	if (wScroll < $(".portfolio").offset().top - 700){
		$(".portfolio .img-thumbnail").removeClass("muncul");
	}
	
	if (wScroll >= 10){
		$(".kiri").addClass("muncul");
		$(".kanan").addClass("muncul");
	}
	
});
