from django.urls import path
from django.views.generic import TemplateView

urlpatterns = [
	path('', TemplateView.as_view(template_name = 'story1/index.html'), name = 'index')
]

app_name = 'story1'