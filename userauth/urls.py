from django.urls import path
from .views import userlogin, userregister, userlogout

urlpatterns = [
	path('login/', userlogin, name = 'login'),
	path('register/', userregister, name = 'register'),
    path('logout/', userlogout, name = 'logout'),
]

app_name = 'userauth'