from django.test import TestCase, Client
from django.urls import resolve

from .views import index

# Create your tests here.
class Story7Test(TestCase):

    # URL Testcases
    def test_url_is_exist_story7(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    # View Testcases
    def test_view_story7_is_using_function_index(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, index)

    # Template Testcases
    def test_template_story7_using_index_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/index.html')