from django import forms
from .models import Matakuliah, Tugas

class MatakuliahForm(forms.ModelForm):
	class Meta:
		model = Matakuliah
		fields = [
			'nama',
			'dosen',
			'sks',
			'deskripsi',
			'semester',
			'tahun',
			'ruang'
		]

		widgets = {
			'nama': forms.TextInput(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan Nama Mata Kuliah'
				}
			),
			'dosen': forms.Select(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan Nama Dosen Pengajar'
				}
			),
			'sks': forms.NumberInput(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan Jumlah SKS'
				}
			),
			'deskripsi': forms.Textarea(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan Deskripsi Mata Kuliah'
				}
			),
			'semester': forms.Select(
				attrs = {
					'class': 'form-control',
				}
			),
			'tahun': forms.Select(
				attrs = {
					'class': 'form-control',
				}
			),
			'ruang': forms.TextInput(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan Ruang Kelas Kuliah'
				}
			)
		}

class TugasForm(forms.ModelForm):
	class Meta:
		model = Tugas
		fields = [
			'nama',
			'matkul',
			'due'
		]

		widgets = {
			'nama': forms.TextInput(
				attrs = {
					'class': 'form-control',
					'placeholder': 'Masukkan Nama Tugas'
				}
			),
			'due': forms.DateTimeInput(
				attrs = {
					'class': 'form-control',
					'type': 'datetime-local'
				}
			)
		}
