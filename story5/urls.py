from django.urls import path
from django.views.generic import TemplateView
from .views import index, tambah, hapus, edit, detail, delete_tugas

urlpatterns = [
	path('', index, name = 'index'),
	path('tambah/', tambah, name = 'tambah'),
	path('hapus/<int:id>/', hapus, name = 'hapus'),
	path('edit/<int:id>/', edit, name = 'edit'),
	path('detail/<int:id>/', detail, name='detail'),
	path('hapus/tugas/<int:id>', delete_tugas, name='delete_tugas'),
]

app_name = 'story5'