from django.contrib import admin
from .models import Matakuliah, Tugas

# Register your models here.
admin.site.register(Matakuliah)
admin.site.register(Tugas)