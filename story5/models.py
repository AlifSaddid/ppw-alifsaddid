from django.db import models
from bs4 import BeautifulSoup
import requests

def get_dosen():
    url = "https://www.cs.ui.ac.id/index.php/staf-pengajar/"
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    content = soup.find("div", class_='gdlr-core-pbf-sidebar-content-inner').find_all("div", class_="gdlr-core-pbf-element")[1]
    content = content.find("div", class_="gdlr-core-personnel-item").find_all("div", class_="gdlr-core-personnel-list-column")

    dosen_list = [i.find("div", class_="gdlr-core-personnel-list").find("h3").find("a").text for i in content]
    dosen_list = [(i, i) for i in dosen_list]

    content = soup.find("div", class_='gdlr-core-pbf-sidebar-content-inner').find_all("div", class_="gdlr-core-pbf-element")[2]
    content = content.find("div", class_="gdlr-core-personnel-item").find_all("div", class_="gdlr-core-personnel-list-column")
    another_dosen = [i.find("div", class_="gdlr-core-personnel-list").find("h3").find("a").text for i in content]
    another_dosen = [(i, i) for i in another_dosen]
    dosen_list.extend(another_dosen)

    url = "https://cs.ui.ac.id/staf-pengajar-tidak-tetap/"
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    content = soup.find("div", class_='gdlr-core-pbf-sidebar-content-inner').find_all("div", class_="gdlr-core-pbf-element")[1]
    content = content.find("div", class_="gdlr-core-personnel-item").find_all("div", class_="gdlr-core-personnel-list-column")
    another_dosen = [i.find("div", class_="gdlr-core-personnel-list").find("h3").find("a").text for i in content]
    another_dosen = [(i, i) for i in another_dosen]

    dosen_list.extend(another_dosen)
    return dosen_list

# Create your models here.
class Matakuliah(models.Model):
    nama        = models.CharField(max_length = 100)
    dosen       = models.CharField(
                    max_length = 500,
                    choices = get_dosen(),
                    default = 'Prof. Achmad Nizar Hidayanto, S.Kom., M.Kom., Dr.'
    )
    sks         = models.IntegerField()
    deskripsi   = models.CharField(max_length = 1000)
    semester    = models.CharField(
                    max_length = 6,
                    choices = [
                        ('Ganjil', 'Ganjil'),
                        ('Genap', 'Genap')
                    ],
                    default = 'Ganjil'
    )
    tahun       = models.CharField(
                    max_length = 9,
                    choices = [
                        ('2019/2020', '2019/2020'),
                        ('2020/2021', '2020/2021'),
                        ('2021/2022', '2021/2022'),
                        ('2022/2023', '2022/2023'),
                    ],
                    default = '2020/2021'
    )
    ruang       = models.CharField(max_length = 100)
    tugas_tersisa = models.IntegerField(default = 0)

    class Meta:
        ordering = ["-tugas_tersisa", "nama"]

    def __str__(self):
    	return "{}. {}".format(self.id, self.nama)

class Tugas(models.Model):
    nama    = models.CharField(max_length = 100)
    matkul  = models.ForeignKey(Matakuliah, on_delete = models.CASCADE)
    due     = models.DateTimeField()

    def __str__(self):
        return "{}. {}".format(self.id, self.nama)

    class Meta:
        ordering = ["due"]
    