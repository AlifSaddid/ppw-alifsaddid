from django.shortcuts import render, redirect, resolve_url
from django.urls import reverse
from django.utils import timezone
from .models import Matakuliah, Tugas
from .forms import MatakuliahForm, TugasForm
from math import ceil
import datetime
import pytz

allHari = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"]

# Create your views here.
def index(request):
	allMatakuliah = Matakuliah.objects.all()
	allTugas = Tugas.objects.all()
	now = datetime.datetime.now(timezone.utc)
	now += datetime.timedelta(hours = 7)
	for tugas in allTugas:
		if (tugas.due < now):
			tugas.matkul.tugas_tersisa -= 1
			tugas.matkul.save()
			tugas.delete()
	hari = [allHari[(now + datetime.timedelta(i)).weekday()] for i in range(0, 12)]
	tanggal = [str((now + datetime.timedelta(i)).day) + '/' + str((now + datetime.timedelta(i)).month) for i in range(0, 12)] 
	delta = [(i.due - now, i.due - now, i.nama) for i in allTugas]
	delta = [[min(i.days + 1 + (1 if ((now + datetime.timedelta(seconds = i.seconds)).weekday() != now.weekday()) else 0), 12), j.days + 1, k] for i, j, k in delta]
	for i in delta:
		if (i[1] <= 2):
			i[1] = "linear-gradient(to right, #D45769, #D45769, #F9AD6A)"
		elif (i[1] <= 4):
			i[1] = "linear-gradient(to right, #F9AD6A, #F9AD6A, #F9E07F)"
		elif (i[1] <= 7):
			i[1] = "linear-gradient(to right, #F9E07F, #F9E07F, #7BE495)"
		else:
			i[1] = "linear-gradient(to right, #7BE495, #7BE495, #86E3CE)"
	context = {
		'allMatakuliah': allMatakuliah,
		'allTugas': delta,
		'allHari': hari,
		'allTanggal': tanggal,
	}
	return render(request, 'story5/index.html', context)

def tambah(request):
	if (request.user.is_anonymous):
		return redirect(resolve_url('userauth:login') + '#login')

	form = MatakuliahForm(request.POST or None)

	if (request.method == "POST"):
		if (form.is_valid()):
			form.save()
		return redirect(resolve_url('story5:index') + '#jadwal')

	context = {
		'Form': form,
	}

	return render(request, 'story5/tambahMatkul.html', context)

def hapus(request, id):
	if (request.user.is_anonymous):
		return redirect(resolve_url('userauth:login') + '#login')
	
	try:
		matkul = Matakuliah.objects.get(id = id)

		if (request.method == "POST"):
			matkul.delete()
			return redirect(resolve_url('story5:index') + '#jadwal')

		context = {
			'matkul': matkul
		}
		return render(request, 'story5/deleteMatkul.html', context)
	except:
		return redirect(resolve_url('story5:index') + '#jadwal')

def edit(request, id):
	if (request.user.is_anonymous):
		return redirect(resolve_url('userauth:login') + '#login')

	matkul = Matakuliah.objects.get(id = id)
	form = MatakuliahForm(initial = {
		'nama': matkul.nama,
		'dosen': matkul.dosen,
		'sks': matkul.sks,
		'deskripsi': matkul.deskripsi,
		'semester': matkul.semester,
		'tahun': matkul.tahun,
		'ruang': matkul.ruang
	}, instance = matkul)

	if (request.method == "POST"):
		form = MatakuliahForm(request.POST, instance = matkul)
		if (form.is_valid()):
			form.save()
		return redirect(resolve_url('story5:index') + '#jadwal')

	context = {
		'Form': form,
	}

	return render(request, 'story5/editMatkul.html', context)

def detail(request, id):
	form = TugasForm(request.POST or None)
	matkul = Matakuliah.objects.get(id = id)
	all_tugas = Tugas.objects.filter(matkul = matkul)
	if (request.method == "POST"):
		now = datetime.datetime.now()
		timeNow = str(now)
		if (timeNow < " ".join(request.POST['due'].split("T"))):
			if (form.is_valid):
				matkul.tugas_tersisa += 1
				matkul.save()
				form.save()
		form = TugasForm()

	context = {
		'matkul': matkul,
		'Form': form,
		'Tugas': all_tugas,
	}

	return render(request, 'story5/detailMatkul.html', context)

def delete_tugas(request, id):
	tugas = Tugas.objects.get(id = id)
	if (request.method == "POST"):
		matkul = tugas.matkul
		matkul.tugas_tersisa -= 1
		matkul.save()
		tugas.delete()
	return redirect('story5:detail', id = tugas.matkul.id)
	