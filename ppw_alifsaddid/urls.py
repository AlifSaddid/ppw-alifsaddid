from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name = 'index.html'), name = 'home'),
    path('story1/', include('story1.urls', namespace='story1')),
    path('story3/', include('story3.urls', namespace='story3')),
    path('story4/', RedirectView.as_view(pattern_name='story3:index'), name='story4'),
    path('story5/', include('story5.urls', namespace='story5')),
    path('story6/', include('story6.urls', namespace='story6')),
    path('story7/', include('story7.urls', namespace='story7')),
    path('story8/', include('story8.urls', namespace='story8')),
    path('auth/', include('userauth.urls', namespace='auth')),
]
