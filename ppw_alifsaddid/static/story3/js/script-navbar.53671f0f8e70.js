const navbar = document.querySelector("nav");
const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
const allNavbars = document.querySelectorAll("ul li a.nav-link");

let previousScroll = 0;

$(window).scroll(function(){
    let wScroll = $(this).scrollTop();

    if (wScroll > previousScroll){
        navbar.classList.add("hide");
        navbar.classList.remove("muncul");
    }

    if (wScroll < previousScroll){
        navbar.classList.add("muncul");
        navbar.classList.remove("hide");
    }

    if (wScroll > (vh - 10)){
        allNavbars.forEach(function(item){
            item.classList.add("green");
        })
        navbar.classList.add("blur-background");
        navbar.classList.add("navbar-light");
        navbar.classList.remove("navbar-dark");
    }

    if (wScroll < (vh - 10)){
        allNavbars.forEach(function(item){
            item.classList.remove("green");
        })
        navbar.classList.remove("blur-background");
        navbar.classList.remove("navbar-light");
        navbar.classList.add("navbar-dark");
    }

    previousScroll = wScroll;
})